import React, { Component } from 'react'
import { Container, Form, Button, Image, Row, Col} from 'react-bootstrap';

import MyTable from './components/MyTable'
import MyHeader from './components/MyHeader'
class App extends Component {
  constructor(){
        super();
        this.state={
            data: [
              {
                id: 1,
                username: "Sengtit",
                email: "sengtit980@gmail.com",
                password: "",
                gender: "Male"
              },
              {
                id: 2,
                username: "Sima",
                email: "sima999@gmail.com",
                password: "",
                gender: "Female"
              }
            ],
            
            username: '',
            gender: '',
            email: ''
        }
        
    }
    getUsername = (event) => {
        console.log('Event: ',event.target.value);
        this.setState({username: event.target.value})
    }
    // handleSubmit = (event) => {
    //   let data = {username: this.state.username}
    //   let newState = [...this.state.data, data] 
    //   this.setState({data:newState})
    //   this.setState({username:''})
    // }
    handleSubmit = event => {
    console.log(this.state.username)
    // console.log(this.state.gender)
    let data = {
          id: this.state.data.length+1,
          username: this.state.username,
          email: this.state.email,
          gender: this.state.gender,
    }
    let getData = [...this.state.data, data]
    this.setState({
      data: getData
    })
    this.setState({username: ""}) ;
  }
    render() {
        console.log(this.data)
        return (
            <Container>
              <MyHeader />
              <Row>
                <Col md={4}>
                <Form className="justify-content-">
                    <div className="my-2 text-center">
                        <Image src="https://upload.wikimedia.org/wikipedia/commons/9/99/Sample_User_Icon.png" 
                            style={{width:100}}
                            roundedCircle />
                    </div>
                    <h1 className="my-2 text-center">User Account</h1>
                        <Form.Label>UserName</Form.Label>
                        <Form.Group controlId="formBasicEmail">                            
                            <Form.Control
                                name="name"
                                type="text"
                                value={this.state.username}
                                onChange={(e) => this.setState({ username: e.target.value })}
                                
                                placeholder="Username"
                            />                        
                        </Form.Group>
                        <Form.Label>Gender</Form.Label>
                        {['radio'].map((type) => (
                        <div key={`inline-${type}`} className="mb-3">
                          <Form.Check 
                          value={this.state.gender}
                          inline label="Female" name="group1" type={type} id={`inline-${type}-1`} />
                          <Form.Check inline label="Male" name="group1" type={type} id={`inline-${type}-1`} />
                        </div>
                        ))}
                        <Form.Label>Email</Form.Label>
                        <Form.Group controlId="formBasicEmail">
                            
                            <Form.Control
                                value={this.state.email}
                                name="email"
                                type="email"
                                placeholder="Email"
                                
                            />
                        
                        </Form.Group>
                        <Form.Label>Password</Form.Label>
                        <Form.Group controlId="formBasicEmail">
                        
                        <Form.Control
                            name="password"
                            type="password"
                            placeholder="Password"
                            
                        />
                        </Form.Group>
                        
                        <Button variant="primary"
                            onClick={()=> this.handleSubmit()}
                        >
                            Submit
                        </Button>
                </Form>
                </Col>
                <Col>
                  <MyTable data={this.state.data} />
                </Col>
                </Row>
                
            </Container>
        )
    }


  
}
export default App;