import React from 'react'
import {Navbar} from 'react-bootstrap';
export default function MyHeader() {
    return (
        <div>
            <Navbar>
                <Navbar.Brand href="#home">KSHRD Students</Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                <Navbar.Text>
                    Signed in as: <a href="#login">Cito</a>
                </Navbar.Text>
                </Navbar.Collapse>
            </Navbar>
        </div>
    )
}
