import React from 'react'
import {Table, Button} from 'react-bootstrap';
export default function MyTable(props) {
    console.log(props.data)
    return (
        <div>
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>#</th>
                    <th>USERNAME</th>
                    <th>EMAIL</th>
                    <th>GENDER</th>
                    
                </tr>
                </thead>
                <tbody>
                {props.data.map((item,idx)=>
                    <tr key={idx} onClick={()=>props.onDeleteMethod(idx)}style={{color:item.isDeleted?"red":"blue"}}>
                    <td>{item.id}</td>
                    <td>{item.username}</td>
                    <td>{item.email}</td>
                    <td>{item.gender}</td>
                    </tr>
                )
                }
                
                </tbody>
            </Table>
            <Button variant="danger">Remove</Button>
        </div>
    )
}
